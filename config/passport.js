var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var crypto = require('crypto');
var Helper = require('../config/helper.js');

module.exports = function (passport) {

// Serializing the user
passport.serializeUser(function (user, done) {
    done(null, user.email);
});

// DeSerializing the user
passport.deserializeUser(function (email, done) {
    User.getUserByEmail(email, function (err, user) {
        done(err, user);
    });
});

passport.use('local-signup', new LocalStrategy({
    passReqToCallback: true
}, function (req, email, password, done) {
    process.nextTick(function () {
        try {
            // Parsing the recieved user object 
            var new_user = JSON.parse(password);
            User.getUserByEmail(new_user.email, function (err, user) {
                
                if (err) {
                    return done(null, false, req.flash('signupMessage', Helper.error_messages.INTERNAL_SERVER));
                }

                if (user.length == 0) {
                    new_user.salt = crypto.randomBytes(16).toString('hex');
                    new_user.hash = crypto.pbkdf2Sync(new_user.password, new_user.salt, 1000, 64).toString('hex');
                    new_user.isHstAdmin = false; // for HST solar Admin
                    User.addUser(new_user, function (err, user) {
                        if (err) {
                            return done(null);
                        } else
                            return done(null, user);
                    })
                } else {
                    user.error = "Email used already.";
                    return done(null, false,req.flash('signupMessage', Helper.error_messages.EMAIL_ALREADY_EXIST));
                }
            })
        } catch (err) {
            return done(null, false,req.flash('signupMessage', Helper.error_messages.INTERNAL_SERVER));
        }
    });
}));

passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        passReqToCallback: true
    }, function (req, email, password, done) {
        process.nextTick(function () {
            try {
                User.getUserByEmail(email, function (err, user) {
                    if (err) {
                        return done(null);
                    }
                    // no user with that email
                    if(!user.length){
                        return done(null, false, req.flash('loginMessage', Helper.error_messages.EMAIL_NOT_EXIST));
                    }

                    var salt = user[0].salt;
                    if (salt) {
                        salt = salt.toString();
                    } else {
                        salt = "";
                    }
              
                    var hash = crypto.pbkdf2Sync(password, salt, 1000, 64).toString('hex');
                    if (user[0].hash == hash) {
                        return done(null, user[0]);
                    } else {
                        // when password doesn't match
                        var user_info = user[0];
                        user_info.error = "Your email or Password are not correct";
                        return done(null, false,req.flash('loginMessage', Helper.error_messages.INVALID_PASSWORD));
                    }
                })
            } catch (err) {
                return done(null,false,req.flash('loginMessage', Helper.error_messages.INTERNAL_SERVER));
            }
        });
    }))
}