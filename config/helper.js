module.exports = {
	//NODE_ENV : "production",
	//NODE_ENV : "development",
	mail_gun : {
		API_KEY : "key-3e4163a64d4d239bf2c3b1e64113e87c",
		SUPPORT_EMAIL : "support@hstsolar.com",
		DOMAIN : "mg.hstsolar.com",
		FROM_SOLAR_SUPPORT_TEAM : "HST Solar Support Team <support@hstsolar.com>",
		HSTSOLAR_SUPPORT_EMAIL : "support@hstsolar.com"
	},
	s3Credential : {
		ACCESS_KEY_ID : 'AKIAJ5E5JBTAUM3MHS5Q',
		SECRET_ACCESS_KEY: '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',
		REGION : 'us-east-1'

	},
	constants : {
		SITE_BOUNDARIES : "Site Boundaries",
		SITE_EXCLUSIONS : "Site Exclusions",
		SITE_PCC : "PCC",
		USER_MANAGE_ROLE : "manager"
	},
	mail_send : {
		CREATE_MANAGER : "create_manager",
		FORGOT_PASSWORD : "forgot_password",
		HELP_SUPPORT : "help_support"
	},
	s3Bucket : {
		dev:{
			WEATHER__FILE_BUCKET : "weatherfile-dev",
			WEATHER_FILE_BASE_URL : 'https://s3.amazonaws.com/weatherfile-dev'
		},
		qa:{
			WEATHER__FILE_BUCKET : "weatherfile-qa",
			WEATHER_FILE_BASE_URL : 'https://s3.amazonaws.com/weatherfile-qa'
		},
		inside:{
			WEATHER__FILE_BUCKET : "weatherfile",
			WEATHER_FILE_BASE_URL : 'https://s3.amazonaws.com/weatherfile'
		}
	},
	optimizer_host : {
		OPTIMIZER_DEV : "dev.api.hstsolar.com",
		OPTIMIZER_QA: "qa.api.hstsolar.com",
		OPTIMIZER_PROD : "inside.api.hstsolar.com"
	},
	logger : {
		logger_levels: {
		    error: 0,
		    debug: 1,
		    warn: 2,
		    data: 3,
		    info: 4,
		    verbose: 5,
		    silly: 6
		  },
		  logger_colors: {
		    error: 'red',
		    debug: 'blue',
		    warn: 'yellow',
		    data: 'grey',
		    info: 'green',
		    verbose: 'cyan',
		    silly: 'magenta'
		  }
	},
	error_messages : {
		INVALID_PASSWORD : "Invalid Password. Please try again with a different email and password or register",
		EMAIL_NOT_EXIST  : "Your email does not seem to be registered. Please register or contact support@hstsolar.com",
		INTERNAL_SERVER : "Internal_Server_Error",
		EMAIL_ALREADY_EXIST : "Email already exists. Please try again with a different email or reset your password"
	},
	//Nirav Kapoor : default setting values which will be set at the time of company creation
	default_company_setting : {
		MAX_PROJECT_COUNT : 1,
		MAX_OPTIMIZATION_COUNT : 10
	},
	//Nirav Kapoor : minimum lines count a uploaded weather file should have
	weather_constant : {
		line_countLimit : 8762
	},
	//Nirav Kapoor : constant value for TMY2 weather file according to there specified rule
	tmy2_constant : {
		TMY2_LINE_COUNTLIMIT : 8761,
		FIRST_ROW_CHARLIMIT : 60,
		SECOND_ROW_CHARLIMIT_122 : 122,
		SECOND_ROW_CHARLIMIT_142 : 143,
		SECOND_ROW : "Date (MM/DD/YYYY),Time (HH:MM),ETR (W/m^2),ETRN (W/m^2),GHI (W/m^2),GHI source,GHI uncert (%),DNI (W/m^2),DNI source,DNI uncert (%),DHI (W/m^2),DHI source,DHI uncert (%),GH illum (lx),GH illum source,Global illum uncert (%),DN illum (lx),DN illum source,DN illum uncert (%),DH illum (lx),DH illum source,DH illum uncert (%),Zenith lum (cd/m^2),Zenith lum source,Zenith lum uncert (%),TotCld (tenths),TotCld source,TotCld uncert (code),OpqCld (tenths),OpqCld source,OpqCld uncert (code),Dry-bulb (C),Dry-bulb source,Dry-bulb uncert (code),Dew-point (C),Dew-point source,Dew-point uncert (code),RHum (%),RHum source,RHum uncert (code),Pressure (mbar),Pressure source,Pressure uncert (code),Wdir (degrees),Wdir source,Wdir uncert (code),Wspd (m/s),Wspd source,Wspd uncert (code),Hvis (m),Hvis source,Hvis uncert (code),CeilHgt (m),CeilHgt source,CeilHgt uncert (code),Pwat (cm),Pwat source,Pwat uncert (code),AOD (unitless),AOD source,AOD uncert (code),Alb (unitless),Alb source,Alb uncert (code),Lprecip depth (mm),Lprecip quantity (hr),Lprecip source,Lprecip uncert (code)"
    },
	//Nirav Kapoor : arcgis api token expiration - 15 days
	arc_gis : {
		ARC_GIS_EXPIRE : 1296000
	}
}