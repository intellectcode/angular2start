"use strict";
var router_1 = require('@angular/router');
var projects_component_1 = require('./projects/projects.component');
var login_component_1 = require('./login/login.component');
var edit_projects_component_1 = require('./projects/edit_projects.component');
// Route Configuration
exports.routes = [
    { path: 'projects', component: projects_component_1.ProjectsComponent },
    { path: '', component: login_component_1.LoginComponent },
    { path: 'projects/edit/:id', component: edit_projects_component_1.EditProjectComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes, { useHash: true });
//# sourceMappingURL=app.routes.js.map