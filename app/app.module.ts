import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }         from './app.component';
import { ProjectsComponent } from './projects/projects.component'
import {LoginComponent} from './login/login.component'
import {EditProjectComponent} from './projects/edit_projects.component'

import { routing } from './app.routes';
import {ProjectService} from './projects/projects.service';
import { LoginService } from './login/login.service';
import { ValidationService } from './shared/validation.service';
import { LocalStorageService, LOCAL_STORAGE_SERVICE_CONFIG } from 'angular-2-local-storage';
import { ValueProvider } from '@angular/core';
import { SharedService } from './shared/utils.service';
import { Routes, RouterModule } from '@angular/router';

import { FocusDirective } from  './directives/focus.directive';
// import { MapService } from './shared/map.service';
const WINDOW_PROVIDER : ValueProvider = {
    provide: Window,
    useValue: window
};

let localStorageServiceConfig = {
    prefix: '',
    storageType: 'localStorage'
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
     HttpModule,
    JsonpModule,
    routing
  ],
  declarations: [
    AppComponent,
    ProjectsComponent,
    EditProjectComponent,
    LoginComponent,
    FocusDirective
  ],
  providers: [
    ProjectService,
    LoginService,
    ValidationService,
    LocalStorageService,
      {
        provide: LOCAL_STORAGE_SERVICE_CONFIG, useValue: localStorageServiceConfig
      },
    WINDOW_PROVIDER,
    SharedService
    // MapService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}

