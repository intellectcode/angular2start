import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './projects/projects.component'
import {LoginComponent} from './login/login.component';
import {EditProjectComponent} from './projects/edit_projects.component';
declare var require : any;
// Route Configuration
export const routes: Routes = [
 { path: 'projects',  component : ProjectsComponent },
 { path: '',  component : LoginComponent },
 { path : 'projects/edit/:id', component : EditProjectComponent}
 // { path: '/projects/edit',  component : LoginComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes,{ useHash: true });
