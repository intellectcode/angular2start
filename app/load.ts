declare var System: any;
declare var esriSystem: any;


// load esri modules needed by this application
// into a SystemJS module called esri-mods
esriSystem.register(
    // array of Esri module names to load and then register with SystemJS 
    [
        'esri/geometry/Point',
        'esri/geometry/geometryEngineAsync',
        'esri/Graphic',
        'esri/layers/FeatureLayer',
        'esri/layers/GraphicsLayer',
        'esri/Map',
        'esri/symbols/SimpleLineSymbol',
        'esri/symbols/SimpleFillSymbol',
        'esri/views/MapView',
        'esri/views/SceneView'
    ],

    // optional callback function 
    function () {
        // then bootstrap application 
        System.import('app')
            .then(null, console.error.bind(console));
    })
    // // optional paramaters 
    // {
    //     // name of SystemJS module that you will import from, defaults to 'esri' 
    //     outModuleName: 'esri-mods'
    //     //// by default each module will use everything after the last '/' in their name 
    //     //// however you can override that for specific modules here 
    //     //moduleNameOverrides: {
    //     //    'esri/request': 'esriRequest'
    //     //}
    // });
    console.log("esriSystem",esriSystem);

