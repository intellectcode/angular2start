"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var user_schema_1 = require('../shared/user.schema');
var core_1 = require('@angular/core');
// export class Token {
// 	// private localStorageService = new  LocalStorageService();
// 	constructor(){}
// 	static saveToken(token:string) : void {
// 		// this.localStorageService.set("mean-token",token);	
// 	}
// 	static getToken() : User {
// 		// let token = this.localStorageService.get("mean-token");
// 		let user = new User();
// 		// if(token){
// 		// 	var payload = token.split('.')[1];
// 		// 	payload = window.atob(payload);
//   		//  user = <User>JSON.parse(payload);
// 		// }
// 		return user;
// 	}
// }
var SharedService = (function () {
    function SharedService() {
        this.userInfo = new user_schema_1.User();
    }
    SharedService.prototype.set = function (data) {
        this.userInfo = data;
    };
    SharedService.prototype.get = function () {
        return this.userInfo;
    };
    SharedService.prototype.addAttr = function (attr, value) {
        this.userInfo[attr] = value;
    };
    SharedService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SharedService);
    return SharedService;
}());
exports.SharedService = SharedService;
var ProjectSharedService = (function () {
    function ProjectSharedService() {
    }
    ProjectSharedService.prototype.set = function (data) {
        this.projectInfo = data;
    };
    ProjectSharedService.prototype.get = function () {
        return this.projectInfo;
    };
    ProjectSharedService.prototype.getSpecificAttr = function (attr) {
        return this.projectInfo[attr];
    };
    ProjectSharedService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ProjectSharedService);
    return ProjectSharedService;
}());
exports.ProjectSharedService = ProjectSharedService;
//# sourceMappingURL=utils.service.js.map