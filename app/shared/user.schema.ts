import { LocalStorageService } from 'angular-2-local-storage';

export class User{
	_id : string;
	email : string;
	password : string;
	first_name : string;
	last_name : string;
	role : string;
	company_id : string;
	company_name : string;
	isHstAdmin : boolean;
}
