import { LocalStorageService } from 'angular-2-local-storage';
import { User } from '../shared/user.schema';
import {Injectable} from '@angular/core';

// export class Token {
// 	// private localStorageService = new  LocalStorageService();

// 	constructor(){}

// 	static saveToken(token:string) : void {
// 		// this.localStorageService.set("mean-token",token);	
// 	}

// 	static getToken() : User {
// 		// let token = this.localStorageService.get("mean-token");
// 		let user = new User();
// 		// if(token){
// 		// 	var payload = token.split('.')[1];
// 		// 	payload = window.atob(payload);
//   		//  user = <User>JSON.parse(payload);
// 		// }
// 		return user;
// 	}
// }

@Injectable()
export class SharedService{
	userInfo = new User();
	 set(data : any) :void{
	 	this.userInfo = data as User;
	 }

	 get(){
	 	return this.userInfo;
	 }

	 addAttr(attr:string,value : any) : void{
	 	this.userInfo[attr] = value;
	 }
}

@Injectable()
export class ProjectSharedService{
	projectInfo : any;
	set(data: any) : void{
		this.projectInfo = data;
	}

	get(){
		return this.projectInfo;
	}

	getSpecificAttr(attr:string){
		return this.projectInfo[attr];
	}
}