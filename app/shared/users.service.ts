import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/operator/map';
@Injectable()
export class UserService{
	constructor (private http : Http) {}

	getWeatherData(lng : any,lat : any) : any {
        return this.http.get('/api/projects/weather-station/'+lng+'/'+lat);
	}
}