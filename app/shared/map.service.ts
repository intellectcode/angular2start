import {Injectable} from '@angular/core';
// import Map = require("esri/map");
// import FeatureLayer=  require('esri/layers/FeatureLayer');
// import InfoTemplate=  require('esri/InfoTemplate');
// import SimpleFillSymbol=  require('esri/symbols/SimpleFillSymbol');
// import SimpleLineSymbol=  require('esri/symbols/SimpleLineSymbol');
// import SimpleMarkerSymbol=  require('esri/symbols/SimpleMarkerSymbol');
// import PictureMarkerSymbol=  require('esri/symbols/PictureMarkerSymbol');
// // import GeometryTask = require( "esri/tasks/geometry");
// // import Geometry = require( "esri/geometry");
// // import Color=  require('dojo/_base/Color');
// import Color=  require('esri/Color');
// import Search = require(  "esri/dijit/Search");
// import Graphic = require( "esri/graphic");
// import GraphicUtils = require( "esri/graphicsUtils");
// // import Array = require( 'dojo/_base/array');
// import Edit = require( "esri/toolbars/edit");
// import Event = require( "dojo/_base/event");
// import GraphicsLayer = require("esri/layers/GraphicsLayer");
// import Polygon = require( "esri/geometry/Polygon");
// import Point = require( "esri/geometry/Point");
// import Extent = require( "esri/geometry/Extent");
// import WebMercator = require( "esri/geometry/webMercatorUtils");
// import Units = require( "esri/units");
// import ElevationProfile = require(  "esri/dijit/ElevationProfile");
// import Draw = require( "esri/toolbars/draw");
// import CartographicLineSymbol = require( "esri/symbols/CartographicLineSymbol");
// import dom = require( "dojo/dom");
// import on = require( "dojo/on");
// import DojoReady = require( "dojo/domReady!");
// import ArcGISImageServiceLayer = require( "esri/layers/ArcGISImageServiceLayer");
// import ImageServiceParameters  = require("esri/layers/ImageServiceParameters");
// import RasterFunction = require( "esri/layers/RasterFunction");
// import Request = require( "esri/request");
// import Basemap = require( "esri/dijit/Basemap");
// import BasemapLayer = require( "esri/dijit/BasemapLayer");
// import Parser = require( "dojo/parser");
// import Config = require( "esri/config");
// import ArcGISTiledMapServiceLayer = require( "esri/layers/ArcGISTiledMapServiceLayer");
// import ArcGISDynamicMapServiceLayer = require( "esri/layers/ArcGISDynamicMapServiceLayer");
// import GeometryService = require( "esri/tasks/GeometryService");
// import AreasAndLengthsParameters = require( "esri/tasks/AreasAndLengthsParameters" );

@Injectable()
export class MapService {
	private wish :  any = {};
	private deps :  any = {
        Map: 'esri/map',
        FeatureLayer: 'esri/layers/FeatureLayer',
        InfoTemplate: 'esri/InfoTemplate',
        SimpleFillSymbol: 'esri/symbols/SimpleFillSymbol',
        SimpleLineSymbol: 'esri/symbols/SimpleLineSymbol',
        SimpleMarkerSymbol: 'esri/symbols/SimpleMarkerSymbol',
        PictureMarkerSymbol: 'esri/symbols/PictureMarkerSymbol',
        GeometryTask : "esri/tasks/geometry",
        Geometry : "esri/geometry",
        Color: 'dojo/_base/Color',
        Search :  "esri/dijit/Search",
        Graphic : "esri/graphic",
        GraphicUtils : "esri/graphicsUtils",
        Array : 'dojo/_base/array',
        Edit : "esri/toolbars/edit",
       	Event : "dojo/_base/event",
        GraphicsLayer :"esri/layers/GraphicsLayer",
        Polygon : "esri/geometry/Polygon",
        Point : "esri/geometry/Point",
        Extent : "esri/geometry/Extent",
        WebMercator : "esri/geometry/webMercatorUtils",
        Units : "esri/units",
		ElevationProfile :	"esri/dijit/ElevationProfile",
		Draw : "esri/toolbars/draw",
		CartographicLineSymbol : "esri/symbols/CartographicLineSymbol",
		dom : "dojo/dom",
		on : "dojo/on",
        DojoReady : "dojo/domReady!",
        ArcGISImageServiceLayer : "esri/layers/ArcGISImageServiceLayer",
		ImageServiceParameters  :  "esri/layers/ImageServiceParameters",
		RasterFunction : "esri/layers/RasterFunction",
		Request : "esri/request",
		Basemap : "esri/dijit/Basemap",
		BasemapLayer : "esri/dijit/BasemapLayer",
		Parser : "dojo/parser",
		Config : "esri/config",
		ArcGISTiledMapServiceLayer : "esri/layers/ArcGISTiledMapServiceLayer",
		ArcGISDynamicMapServiceLayer : "esri/layers/ArcGISDynamicMapServiceLayer",
		GeometryService : "esri/tasks/GeometryService",
		AreasAndLengthsParameters : "esri/tasks/AreasAndLengthsParameters"
     };

     loadDependcy() : void{
        // console.log(obj);
     	// Object.keys(this.deps).map(e => {
	     // 	this.wish[e] = require(this.deps[e]);
	     // })
     }
     
     get() : any {
     	return {
            "Map" : Map,

        };
     }

}