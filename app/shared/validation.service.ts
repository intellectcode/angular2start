// import { Control } from "@angular/common";

export class ValidationService {
	
	static checkEmailFormat(control:any){
		if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(control.value)))
		{
			return {"invalidFormat" : true}
		} 
		return null;
	}
}

