// import { Control } from "@angular/common";
"use strict";
var ValidationService = (function () {
    function ValidationService() {
    }
    ValidationService.checkEmailFormat = function (control) {
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(control.value))) {
            return { "invalidFormat": true };
        }
        return null;
    };
    return ValidationService;
}());
exports.ValidationService = ValidationService;
//# sourceMappingURL=validation.service.js.map