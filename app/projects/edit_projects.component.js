"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var projects_service_1 = require('../projects/projects.service');
var login_service_1 = require('../login/login.service');
var users_service_1 = require('../shared/users.service');
var EditProjectComponent = (function () {
    function EditProjectComponent(route, projectService, loginService, userService) {
        this.route = route;
        this.projectService = projectService;
        this.loginService = loginService;
        this.userService = userService;
        this.averageSoilingParam = 0;
        this.polygonBoundaryArea = [];
        this.infoWindows = [];
        this.drawMarker = [];
    }
    EditProjectComponent.prototype.ngOnInit = function () {
        var _this = this;
        // console.log("MapService",MapService);
        this.route.params.subscribe(function (params) {
            _this.project_id = params['id'];
            console.log("project_id", _this.project_id);
        });
        this.getProject(this.project_id);
    };
    EditProjectComponent.prototype.getProject = function (_id) {
        var _this = this;
        this.projectService.getProjectDetail(_id).subscribe(function (proj) {
            var response = JSON.parse(proj._body);
            _this.project = response;
            _this.setupDynamicFields();
            _this.setAverageValue();
            _this.getUserInfo(_this.user_id);
            _this.numbers_to_strings();
            _this.getReports();
            _this.geocodeAddress(_this.project.location, function () {
                this.loadBoundary();
                this.loadExclusions();
                this.loadPointofInterconnect();
                this.addWeatherService();
                //set location to display
                if (this.project.city != '') {
                    if (this.project.state != '') {
                        this.project.showLocation = this.project.city + ', ' + this.project.state;
                    }
                    else {
                        this.project.showLocation = this.project.city + ', ' + this.project.country;
                    }
                }
                else {
                    if (this.project.state != '') {
                        this.project.showLocation = this.project.state + ', ' + this.project.country;
                    }
                    else {
                        this.project.showLocation = this.project.country;
                    }
                }
                // activate/deactivate buttons (remove PCC)
                if (this.project.site_PCC[1]) {
                    this.pccAdded = true; //added boundary check
                }
                else {
                    this.pccAdded = false; //added boundary check
                }
            });
            if (_this.project.output.current_state == undefined) {
                _this.project.output.current_state = "Finished";
            }
            if (_this.project.output.current_state == "InProgress") {
                _this.optimization_in_progress = true;
            }
            else {
                _this.optimization_in_progress = false;
            }
            if (_this.project.output.current_state == undefined || _this.project.output.current_state == null) {
                _this.project.output.current_state == "Finished";
            }
        });
    };
    EditProjectComponent.prototype.geocodeAddress = function (address, callback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                this.ctrLat = results[0].geometry.location.lat();
                this.ctrLng = results[0].geometry.location.lng();
                this.mapInit(this.ctrLat, this.ctrLng, 16);
                var img = {
                    url: '../img/location.png'
                };
                var marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    map: this.map,
                    icon: img,
                    title: "Project Location"
                });
                this.map.panTo(marker.position);
                callback();
            }
            else {
            }
        });
    };
    EditProjectComponent.prototype.getReports = function () {
        var optimizer_count = this.project.optimizer_count;
        var projectReports = [];
        //optimizer_count will be from n to 1, as 0th value will be for the report which are created before the result of optimization
        while (optimizer_count > 0) {
            var inc_add_sens_plots = (this.project.optimizer_report_status) ? ((this.project.optimizer_report_status[optimizer_count]) ? this.project.optimizer_report_status[optimizer_count].inc_add_sens_plots : 2) : 2;
            var inc_core_sens_plots = (this.project.optimizer_report_status) ? ((this.project.optimizer_report_status[optimizer_count]) ? this.project.optimizer_report_status[optimizer_count].inc_core_sens_plots : 2) : 2;
            projectReports.push({
                folder_id: this.project._id + "-" + optimizer_count,
                name: "Report-" + optimizer_count,
                IS_add_sens_plots: (inc_add_sens_plots == 1) ? true : false,
                IS_core_sens_plots: (inc_core_sens_plots == 1) ? true : false // Nirav Kapoor : whether to show the core plots report url
            });
            optimizer_count--;
        }
    };
    EditProjectComponent.prototype.numbers_to_strings = function () {
        this.project.land_is_owned = String(this.project.land_is_owned);
        this.project.pcc_coinc_pod = String(this.project.pcc_coinc_pod);
        this.project.use_pps = String(this.project.use_pps);
        this.project.optimization_metric = String(this.project.optimization_metric);
        this.project.rack_type = String(this.project.rack_type);
        this.project.gen_mod_ori = String(this.project.gen_mod_ori);
        this.project.exposure_category = String(this.project.exposure_category);
        this.project.depr_fed_type = String(this.project.depr_fed_type);
        this.project.depr_sta_type = String(this.project.depr_sta_type);
        this.project.subarray1_shade_mode_1x = String(this.project.subarray1_shade_mode_1x);
    };
    /**********************************************************************************/
    /******************************* fetch user's info ********************************/
    /**********************************************************************************/
    EditProjectComponent.prototype.getUserInfo = function (user_id) {
        var _this = this;
        if (user_id) {
            this.loginService.getUserInfo(user_id).subscribe(function (user) {
                _this.currentUser = JSON.parse(user._body);
            }).error(function (err) {
                console.log('error: ' + err); //some problem
            });
        }
    };
    EditProjectComponent.prototype.setupDynamicFields = function () {
        this.show_concrete_cost = false;
        if (this.project.portable_inverter_db.length > 0) {
            for (var i = 0; i < this.project.portable_inverter_db.length; i++) {
                if (this.project.portable_inverter_db[i].use_prefabricated_inv_station == 0) {
                    this.show_concrete_cost = true;
                }
            }
        }
    };
    //set average value for soiling params
    EditProjectComponent.prototype.setAverageValue = function () {
        var sum = 0;
        for (var i = 0; i < this.project.subarray1_soiling.length; i++) {
            sum += parseInt(this.project.subarray1_soiling[i]); //calculate average value
        }
        this.averageSoilingParam = sum / this.project.subarray1_soiling.length; //calculate average
    };
    EditProjectComponent.prototype.addWeatherService = function () {
        var _this = this;
        var img = {
            url: '../img/weather-station.png'
        };
        var lat = this.map.getCenter().lat();
        var lng = this.map.getCenter().lng();
        //Nirav Kapoor : get weather station depending on the map center coordinates
        this.userService.getWeatherData(lng, lat).subscribe(function (response) {
            var data = JSON.parse(response._body);
            if (data.error) {
            }
            else {
                data = data.weather;
                _this.stations = [{
                        id: "",
                        city: "No station selected",
                        state: "",
                        elevation: "",
                        distance: "",
                        dataset: ""
                    }];
                for (var i = 0; i < data.length && i < 5; i++) {
                    _this.stations.push({
                        id: data[i]['ID'],
                        city: data[i].station_name,
                        state: data[i].state,
                        lat: data[i].lat,
                        lng: data[i].long,
                        elevation: data[i].elevation,
                        distance: data[i].distance,
                        dataset: ""
                    });
                    // display markers for all weather stations
                    _this.drawMarker[i] = new google.maps.Marker({
                        position: new google.maps.LatLng(data[i].lat, data[i].long),
                        icon: img
                    });
                    // map.setZoom(10);
                    _this.drawMarker[i].setMap(_this.map);
                }
            }
        });
    };
    EditProjectComponent = __decorate([
        core_1.Component({
            templateUrl: './app/projects/edit_projects.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, projects_service_1.ProjectService, login_service_1.LoginService, users_service_1.UserService])
    ], EditProjectComponent);
    return EditProjectComponent;
}());
exports.EditProjectComponent = EditProjectComponent;
//# sourceMappingURL=edit_projects.component.js.map