import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/operator/map';
@Injectable()
export class ProjectService {
	constructor (private http: Http) {}
	private projectsUrl = '/api/projects/user_projects/';

	getUserProjects(id: string) : any {
		return this.http.get(this.projectsUrl + id)
						// .map((res:Response) => res.json())
						// .catch((error : any) => console.log(error))
	}

	getProjectDetail(id:string) : any {
		return this.http.get('/api/projects/'+id);
	}
}