"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
// import { Router } from '@angular/router';
var projects_service_1 = require('../projects/projects.service');
var utils_service_1 = require('../shared/utils.service');
// import { MapService } from '../shared/map.service';
var ProjectsComponent = (function () {
    function ProjectsComponent(projectService, sharedService) {
        this.projectService = projectService;
        this.sharedService = sharedService;
        this.projects = [];
        this.myFocusTriggeringEventEmitter = new core_1.EventEmitter();
    }
    ProjectsComponent.prototype.ngOnInit = function () {
        // reset login status
        this.getProjects();
        // this.mapService.loadDependcy();
        // let temp : any = this.mapService.get();
        // console.log("MapService",temp);
    };
    ProjectsComponent.prototype.getProjects = function () {
        var _this = this;
        var user = this.sharedService.get();
        if (user._id) {
            // this.id = "5832e0b253c7240a4cb6d8f7";
            this.projectService.getUserProjects(user._id).subscribe(function (proj) {
                var response = JSON.parse(proj._body);
                _this.projects = response.projects;
                _this.adminName = response.admin.first_name + ' ' + response.admin.last_name;
                console.log(_this.projects);
            }, function (err) {
                console.log(err);
            });
        }
        else {
            window.location.href = "/#/";
        }
    };
    ProjectsComponent.prototype.report_url = function () {
    };
    ProjectsComponent.prototype.setFocus = function () {
        this.myFocusTriggeringEventEmitter.emit(true);
    };
    ProjectsComponent.prototype.archiveProject = function () { };
    ProjectsComponent = __decorate([
        core_1.Component({
            templateUrl: './app/projects/projects.Component.html'
        }), 
        __metadata('design:paramtypes', [projects_service_1.ProjectService, utils_service_1.SharedService])
    ], ProjectsComponent);
    return ProjectsComponent;
}());
exports.ProjectsComponent = ProjectsComponent;
//# sourceMappingURL=projects.component.js.map