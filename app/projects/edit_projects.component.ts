import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ProjectService} from '../projects/projects.service';
import {ProjectSharedService} from '../shared/utils.service';
import { LoginService} from '../login/login.service';
import { MapService } from '../shared/map.service';
import {UserService} from '../shared/users.service';

declare var google: any;
@Component({
	templateUrl : './app/projects/edit_projects.component.html'
})

export class EditProjectComponent implements OnInit {

	constructor(private route: ActivatedRoute,private projectService : ProjectService,private loginService: LoginService,private userService : UserService){

	}

	private project_id : string;
	private project : any;
	private show_concrete_cost : boolean;
	private optimization_in_progress : boolean;
	private averageSoilingParam : number = 0;
	private weatherType : string;
	private mapOptions :any;
	private lat : number;
	private lng :number;
	private polygonBoundaryArea : Array<any> = [];
	private map : any;
	private totalSurfaceBound : number;
	private totalSurfaceExcl : number;
	private totalAreaf2Excl : number;	
	private polygonExclusionAreas : any;
	private delBoundary : boolean;
	private addBoundarySetback : boolean;
	private addExclusionSetback : boolean;
	private delExclusion : boolean;
	private infoWindows : Array<any> = [];
	private ctrLat : number;
	private ctrLng : number;
	private origLat : number;
	private origLng : number;
	private editToolbar : any;
	private polygonGraphic : any;
	private onBoundary : boolean;
	private onExclusion : boolean;
	private singleBoundary : boolean;
	private addToSurfaceArea : boolean;
	private editSurfaceArea : boolean;
	private user_id : string;
	private currentUser : any;
	private stations : any;
	private drawMarker : Array<any> = [];
	ngOnInit(){
		// console.log("MapService",MapService);
		this.route.params.subscribe(params=> {
			this.project_id = params['id'];
			console.log("project_id",this.project_id);	
		})
		this.getProject(this.project_id);
	}

	private getProject(_id:string):any {
		this.projectService.getProjectDetail(_id).subscribe(proj =>{
			let response = JSON.parse(proj._body);
			this.project = response;
			this.setupDynamicFields();
			this.setAverageValue();
			this.getUserInfo(this.user_id);
			this.numbers_to_strings();
			this.getReports();
			this.geocodeAddress(this.project.location,function(){

				this.loadBoundary();
			 	this.loadExclusions();
			 	this.loadPointofInterconnect();
			 	this.addWeatherService();

			 	//set location to display
			 	if(this.project.city != ''){//check for city name in location
			 		if(this.project.state != ''){
			 			this.project.showLocation = this.project.city+', '+this.project.state;
			 		}else{
			 			this.project.showLocation = this.project.city+', '+this.project.country;
			 		}
			 	}else{
			 		if(this.project.state != ''){//check for state name in location
			 			this.project.showLocation = this.project.state+', '+this.project.country;
			 		}else{//check for country name in location
			 			this.project.showLocation = this.project.country;
			 		}
			 	}
		 		// activate/deactivate buttons (remove PCC)
		 		if (this.project.site_PCC[1]) {
		 			this.pccAdded = true;//added boundary check
		 		} else {
		 			this.pccAdded = false;//added boundary check
		 		}
		 	});
			if (this.project.output.current_state == undefined) {
				this.project.output.current_state = "Finished";	
			}

			if (this.project.output.current_state == "InProgress") {
				this.optimization_in_progress = true;
			} else {
				this.optimization_in_progress = false;
			}

			if(this.project.output.current_state == undefined || this.project.output.current_state == null){
				this.project.output.current_state == "Finished";	
			}

		})
	}

	private geocodeAddress(address,callback) : void{

		let geocoder = new google.maps.Geocoder();
		geocoder.geocode({'address': address},
			function (results,status){
				if (status == google.maps.GeocoderStatus.OK) {

					this.ctrLat = results[0].geometry.location.lat();
					this.ctrLng = results[0].geometry.location.lng();

					this.mapInit(this.ctrLat,this.ctrLng,16);


					var img = {
						url: '../img/location.png'
					}

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: this.map,
						icon: img,
						title: "Project Location"
					});
					this.map.panTo(marker.position);
					callback();


				} else {
					// $("#invalid-address-modal").modal("show");

				}
			}
		);
	}

	private getReports() : void{
		let optimizer_count = this.project.optimizer_count;
		let projectReports : Array<any> = [];
		//optimizer_count will be from n to 1, as 0th value will be for the report which are created before the result of optimization
		while(optimizer_count > 0){
			let inc_add_sens_plots = (this.project.optimizer_report_status) ? ((this.project.optimizer_report_status[optimizer_count]) ?this.project.optimizer_report_status[optimizer_count].inc_add_sens_plots : 2) : 2;
			let inc_core_sens_plots = (this.project.optimizer_report_status) ? ((this.project.optimizer_report_status[optimizer_count]) ?this.project.optimizer_report_status[optimizer_count].inc_core_sens_plots : 2) : 2;
			projectReports.push(
				{
					folder_id : this.project._id + "-"+optimizer_count,
					name : "Report-"+optimizer_count,
					IS_add_sens_plots : (inc_add_sens_plots == 1)? true : false , // Nirav Kapoor : whether to show the add plots report url
					IS_core_sens_plots : (inc_core_sens_plots == 1)? true : false // Nirav Kapoor : whether to show the core plots report url
				});
			optimizer_count --;
		}
	}

	private numbers_to_strings() : void {
		this.project.land_is_owned = String(this.project.land_is_owned);
		this.project.pcc_coinc_pod = String(this.project.pcc_coinc_pod);
		this.project.use_pps = String(this.project.use_pps);
		this.project.optimization_metric = String(this.project.optimization_metric);
		this.project.rack_type = String(this.project.rack_type);
		this.project.gen_mod_ori = String(this.project.gen_mod_ori);
		this.project.exposure_category = String(this.project.exposure_category);
		this.project.depr_fed_type = String(this.project.depr_fed_type);
		this.project.depr_sta_type = String(this.project.depr_sta_type);
		this.project.subarray1_shade_mode_1x = String(this.project.subarray1_shade_mode_1x);

	}

	/**********************************************************************************/
	/******************************* fetch user's info ********************************/
	/**********************************************************************************/
	private getUserInfo(user_id : string) : void {
	    if(user_id){
	        this.loginService.getUserInfo(user_id).subscribe(user =>{
	        	this.currentUser = JSON.parse(user._body);
	        }).error(function(err) {
	        	console.log('error: '+err);//some problem
	        });
	    }
	}

	private setupDynamicFields() : void{
		this.show_concrete_cost = false;
		if (this.project.portable_inverter_db.length>0){
			for (let i = 0; i < this.project.portable_inverter_db.length; i++){
				if (this.project.portable_inverter_db[i].use_prefabricated_inv_station == 0) {
					this.show_concrete_cost = true;
				}
			}
		}
	}

	//set average value for soiling params
	private setAverageValue(){
		let sum = 0;
		for(let i = 0; i < this.project.subarray1_soiling.length; i++){
	   	 	sum += parseInt(this.project.subarray1_soiling[i]); //calculate average value
	   	}
	    this.averageSoilingParam = sum/this.project.subarray1_soiling.length;//calculate average
	}

	private addWeatherService() : void{
		let img : any = {
					url: '../img/weather-station.png'
		}

		 let lat : any = this.map.getCenter().lat();
		 let lng : any = this.map.getCenter().lng();
		 //Nirav Kapoor : get weather station depending on the map center coordinates
		 this.userService.getWeatherData(lng,lat).subscribe(response =>{
		 	let data = JSON.parse(response._body);
		 	if(data.error){
		 		// $scope.showAlert("Error Loading the Weather Stations");
		 	}
		 	else
		 	{
		 		data = data.weather;
		 		this.stations = [{
		 							id: "",
            						city: "No station selected",
            						state: "",
            						elevation:"",
            						distance:"",
            						dataset:"" 
            				}];

		            for (let i=0;i<data.length && i < 5;i++){
		            	this.stations.push({
		            		id: data[i]['ID'],
		            		city: data[i].station_name,
		            		state: data[i].state,
		            		lat: data[i].lat,
		            		lng: data[i].long,
		            		elevation: data[i].elevation,
		            		distance: data[i].distance,
		            		dataset: ""
		            	});

		            	// display markers for all weather stations
						this.drawMarker[i] = new google.maps.Marker({
						    position: new google.maps.LatLng(data[i].lat,data[i].long),
						    icon: img
						 });
						// map.setZoom(10);
						this.drawMarker[i].setMap(this.map);
		            }
		 	}
		 })
	}

	// //update soiling param
	// private setSoilingParam(element){
	// 	this.project.subarray1_soiling = [];//create subarray again
	// 	for(let i = 0; i < 12; i++){
	//    	 	this.project.subarray1_soiling.push(element.averageSoilingParam);//push the indiviual value
	//    	}
	//    	this.setAverageValue();//set average valeu again
	// }

	// private setWeatherType(){
	// 	this.weatherType = 'Get-Weather-Data';
	// }

	// private resetMap(){//reset map to original settings
	// 	this.geocodeAddress(this.project.location,function(){//load map with  kml boundaries
	// 		this.loadBoundary();
	// 	 	this.loadExclusions();
	// 	 	this.loadPointofInterconnect();
	// 	})
	// 	this.project.weather_station_id = '';//reset weather params
	// 	this.project.weatherStationSelected.lat = '';
	//     this.project.weatherStationSelected.lng = '';
	//     this.project.weatherStationSelected.elevation = '';
	// }

	// /****************************************************************************************/
	// /****************************** Address Geocoding ***************************************/
	// /****************************************************************************************/

	// private geocodeAddress(address : any,callback : any){

	// 	let geocoder = new google.maps.Geocoder();
	// 	geocoder.geocode({'address': address},
	// 		(results,status) => {
	// 			if (status == google.maps.GeocoderStatus.OK) {
	// 				this.ctrLat = results[0].geometry.location.lat();
	// 				this.ctrLng = results[0].geometry.location.lng();
	// 				this.origLat = results[0].geometry.location.lat();
	// 				this.origLng = results[0].geometry.location.lng();
	// 				this.initializeMap(this.ctrLat,this.ctrLng,16,callback,results[0].geometry.location);
	// 			} else {
	// 				// $("#invalid-address-modal").modal("show");
	// 			}
	// 		}
	// 	);
	// }


	// /****************************************************************************************/	
	// /******************************* Initialize Map *****************************************/
	// /****************************************************************************************/

	// private initializeMap(lat,lon,zoom,callback,location){//initialise map div after dom loads
	// 	// Bind map to div element mapDiv
	// 	// Initializes map
	// 	this.mapOptions = {
	// 		center: new google.maps.LatLng(lat,lon),
	// 		zoom: zoom,
	// 		mapTypeId: google.maps.MapTypeId.HYBRID,
	// 		disableDefaultUI: true,
	// 		zoomControl: true,
	// 		zoomControlOptions: {
	// 			style: google.maps.ZoomControlStyle.SMALL,
	// 			position: google.maps.ControlPosition.TOP_RIGHT
	// 		},
	// 		panControl: true,
	// 		mapTypeControl: false,
	// 		scaleControl: true,
	// 		streetViewControl: true,
	// 		rotateControl: true,
	// 		overviewMapControl: true
	// 	};

	//     this.map = new google.maps.Map(document.getElementById('mapDiv'), this.mapOptions);

	//     this.lat = this.map.getCenter().lat();
	// 	this.lng = this.map.getCenter().lng();

	// 	let img = {
	// 		url: '../img/location.png'
	// 	}
		
	// 	let marker = new google.maps.Marker({
	// 		position: location,
	// 		map: this.map,
	// 		icon: img,
	// 		title: "Project Location"
	// 	});
		
	// 	this.map.panTo(marker.position);
		
	// 	callback();//calling the methods
	// }


	// /****************************************************************************************/
	// /*************************** Site Boundaries ********************************************/
	// /****************************************************************************************/

	// private loadBoundary() {//load multiple boundaries

	// 	let number_of_boundaries = this.project.site_boundaries.length;
	// 	// polygonBoundaryArea = [];
	// 	let polygonBoundaryCoords : Array<any> = [];
	// 	if(number_of_boundaries>0 && typeof(this.project.site_boundaries[0].path[0]) != 'undefined'){
	// 		for (var i=0; i < number_of_boundaries; i++) {

	// 			// load empty arry to collect lat lon coordinates for all exclusions
	// 			polygonBoundaryCoords[i]=[];

	// 			// parse exclusions lat lon coordinates
	// 			for (var j=0; j < this.project.site_boundaries[i].path.length; j=j+2){//changed j+2
	// 				polygonBoundaryCoords[i].push(new google.maps.LatLng(this.project.site_boundaries[i].path[j+1],this.project.site_boundaries[i].path[j]));
	// 			}

	// 			this.polygonBoundaryArea[i] = new google.maps.Polygon({
	// 				path: polygonBoundaryCoords[i],
	// 				draggable: true,
	// 				editable: true,
	// 				strokeColor: '#4fff2f',
	// 				strokeOpacity: 1.0,
	// 				strokeWeight: 1,
	// 				fillColor: '#4fff2f',
	// 				fillOpacity: 0.3,
	// 				zIndex:3
	// 			});
	// 			//set setback if it already has or to zero
	// 			this.polygonBoundaryArea[i].setback = this.project.site_boundaries[i].setback || 0;
	// 			//set area id
	// 			let obj = {
	// 		        'id':i
	// 		    };
	// 		    this.polygonBoundaryArea[i].objInfo = obj;
	// 			//set info window for setback
	// 			google.maps.event.addListener(this.polygonBoundaryArea[i], 'click', function(event) {
	// 				this.showSetBackInfoWindow(this,'boundary',event);
	// 			});
	// 			// update object
	// 			// this.polygonBoundaryArea[i].addListener('mousemove', function () {
	// 		 // 	   this.getSurfaceArea(this,'boundary');
	// 		 //    });
	// 			this.polygonBoundaryArea[i].setMap(this.map);
	// 			//this.polygonBoundaryArea[i].addListener('dblclick', selectBoundary);
	// 			this.polygonBoundaryArea[i].addListener('click', this.selectBoundary);
	// 			// this.totalSurfaceArea('boundary',this.polygonBoundaryArea[i]);
	// 			// let area = this.getPolygonArea(this.polygonBoundaryArea[i])//set area on polgon
	// 			// this.polygonBoundaryArea[i].area = area['area'];//set area in acres
	//    //  		this.polygonBoundaryArea[i].areaf2 = area['areaf2'];//in metres
	// 		}

	// 	}
	// 	if(typeof(this.project.site_boundaries) != 'undefined' && typeof(this.project.site_boundaries[0]) != 'undefined' && typeof(this.project.site_boundaries[0].path) != 'undefined' && typeof(this.project.site_boundaries[0].path[0]) != 'undefined' && typeof(this.project.site_boundaries[0].path[1]) != 'undefined'){
	// 		let center = new google.maps.LatLng(this.project.site_boundaries[0].path[1],this.project.site_boundaries[0].path[0]);
	// 		this.map.panTo(center);//pan to polygon position
	// 	}
	// 	else{
	// 		if(typeof(this.project.site_exclusions) != 'undefined' && typeof(this.project.site_exclusions[0]) != 'undefined' && typeof(this.project.site_exclusions[0].path) != 'undefined' && typeof(this.project.site_exclusions[0].path[0]) != 'undefined' && typeof(this.project.site_exclusions[0].path[1]) != 'undefined'){
	// 			let center = new google.maps.LatLng(this.project.site_exclusions[0].path[1],this.project.site_exclusions[0].path[0]);
	// 			this.map.panTo(center);//pan to polygon position
	// 		}
	// 		else{
	// 			if(typeof(this.project.site_PCC) != 'undefined' && typeof(this.project.site_PCC[0]) != 'undefined' && typeof(this.project.site_PCC[1]) != 'undefined'){
	// 				let center = new google.maps.LatLng(this.project.site_PCC[1],this.project.site_PCC[0]);
	// 				this.map.panTo(center);//pan to polygon positio
	// 			}
	// 		}
	// 	}
	// }


	// private drawBoundary(){//to draw polygons of boundary
	//     let center = this.map.extent.getCenter();//get map center
	//     let lat = center.y;
	//     let lon = center.x;
	// 	let latOffset = 100;// Create a big Polygon
	// 	let lonOffset = 100;

	// 	let points = [//set polygon rings for boundary draw
	// 		new mapService.Point(lon - lonOffset, lat + latOffset),
	// 		new mapService.Point(lon, lat+  latOffset),
	// 		new mapService.Point(lon , lat),
	// 		new mapService.Point(lon - lonOffset, lat),
	// 		new mapService.Point(lon - lonOffset, lat + lonOffset)
	// 	];

	//     let polygon = new mapService.Polygon();//create new polygon object
	//     polygon.addRing(points);//set rings of polygon
	//     polygon.spatialReference = this.map.spatialReference;
	//     // Add the polygon to map
	// 	let symbol = new mapService.SimpleFillSymbol().setStyle(mapService.SimpleFillSymbol.STYLE_SOLID).setColor(new mapService.Color([79,255,47,0.3])).setOutline(new mapService.SimpleLineSymbol(
	// 	    mapService.SimpleLineSymbol.STYLE_SOLID,
	// 	    new mapService.Color("#4fff2f"), 2
	// 	  ));//set properties of the polygon
	//     this.polygonGraphic = new mapService.Graphic(polygon, symbol);
	//     //set area id
	//     let id = 'boundary_'+Math.random(5);
	//     let polyLayer = new mapService.GraphicsLayer({id : id});
	//     let length = this.polygonBoundaryArea.length;
	//     this.polygonBoundaryArea[length] = {};
	//     this.polygonBoundaryArea[length][id] = polyLayer;

	//    	this.map.addLayer(polyLayer);//add polygon graphic layer to the map
	//     polyLayer.add(this.polygonGraphic);
	//     this.getPolygonArea(polyLayer.graphics[0].geometry,'boundary','add');//get area of selected polygon
	// }

	// private getSurfaceArea(polygonBoundaryArea,type){

	// 	// calculate surface area within site boundary
	// 	let arr = this.getPolygonArea(polygonBoundaryArea);

	// 	if(!polygonBoundaryArea.area){
	// 		polygonBoundaryArea.area = 0;
	// 	}

	// 	if(!polygonBoundaryArea.areaf2){
	// 		polygonBoundaryArea.areaf2 = 0;
	// 	}

	// 	if(type == 'boundary'){//update total surface area of boundary
	// 		$("#boundary_surface_area_m2").html(arr['area'] + " acres" + " (" + arr['areaf2'] + " m<sup>2</sup>)");
	// 		this.totalSurfaceBound = (Number(this.totalSurfaceBound) - Number(polygonBoundaryArea.area) + Number(arr['area'])).toFixed(2);
	// 		this.totalAreaf2Bound = (Number(this.totalAreaf2Bound) - Number(polygonBoundaryArea.areaf2) + Number(arr['areaf2'])).toFixed(1);
	// 		// $timeout(angular.noop);
	// 	} else if(type == 'exclusion'){//update total surface area of exclusion
	// 		$("#exclusion_surface_area_m2").html(arr['area'] + " acres" + " (" + arr['areaf2'] + " m<sup>2</sup>)");
	// 		this.totalSurfaceExcl = (Number(this.totalSurfaceExcl) - Number(polygonBoundaryArea.area) + Number(arr['area'])).toFixed(2);
	// 		this.totalAreaf2Excl = (Number(this.totalAreaf2Excl) - Number(polygonBoundaryArea.areaf2) + Number(arr['areaf2'])).toFixed(1);
	// 		// $timeout(angular.noop);
	// 	}

	// 	polygonBoundaryArea.area = arr['area'];
	//     polygonBoundaryArea.areaf2 = arr['areaf2'];
	// }

	// private getPolygonArea(polygon,from,add){
	// 	this.onBoundary = true;//set params for output area set
	// 	this.onExclusion = false;
	// 	if(from == 'boundary'){
	// 		polygon = [polygon];
	// 		this.singleBoundary = true;
	// 	}else{
	// 		this.singleBoundary = false;
	// 	}
	// 	if(add == 'add'){
	// 		this.addToSurfaceArea = true;
	// 		this.editSurfaceArea = false;
	// 	}else if(add == 'edit'){
	// 		this.addToSurfaceArea = false;
	// 		this.editSurfaceArea = true;
	// 	}else if(add == 'nothing'){
	// 		this.addToSurfaceArea = false;
	// 		this.editSurfaceArea = false;
	// 	}
	// 	geometryService.simplify(polygon, function(simplifiedGeometries) {
	// 		areasAndLengthParams.polygons = simplifiedGeometries;
	// 		geometryService.areasAndLengths(areasAndLengthParams);
	// 	});
	// }

	// private selectBoundary(event : any){
	// 	if(this.delBoundary){
	// 		this.editToolbar.deactivate();//deactivate editing on polygon
	// 		let pointer = getIndexIfObjWithOwnAttr(this.polygonBoundaryArea,event.graphic._graphicsLayer.id);
	// 	    this.map.removeLayer(event.graphic._graphicsLayer);
	// 	    this.polygonBoundaryArea.splice(pointer,1);
	// 	    this.project.site_boundaries.splice(pointer,1);
	// 		$timeout(angular.noop);
	// 	}

	// }

	// private lockExclusions(){

	// 	let mapBoundaryOpt = {
	// 		draggable: true,
	// 		editable: true,
	// 		zIndex:3
	// 	};

	// 	//added code for polygon boudary area initial
	// 	if(typeof(this.polygonBoundaryArea) == 'undefined'){//set polygon options if it does not exist already
	// 		this.polygonBoundaryArea = new google.maps.Polygon({
	// 			//path: polygonBoundaryCoords,
	// 			draggable: true,
	// 			editable: true,
	// 			strokeColor: '#b22222',
	// 			strokeOpacity: 1.0,
	// 			strokeWeight: 1,
	// 			fillColor: '#b22222',
	// 			fillOpacity: 0.8,
	// 			zIndex:4
	// 		});
	// 	}

	// 	for(let i=0;i< this.polygonBoundaryArea.length;i++){
	// 		this.polygonBoundaryArea[i].setOptions(mapBoundaryOpt);
	// 	}

	// 	let mapExclisionOpt = {
	// 			draggable: false,
	// 			editable:false,
	// 			mapTypeControl: false,
	// 			zIndex:1
	// 	};

	// 	if(typeof(this.polygonExclusionAreas) != 'undefined'){//checking of areas array exist
	// 		for(let i=0;i<this.polygonExclusionAreas.length;i++){
	// 			this.polygonExclusionAreas[i].setOptions(mapExclisionOpt);
	// 		}
	// 	}

	// 	this.finished('exclusion');
	// }

	// private lockBoundaries(){

	// 	let mapBoundaryOpt = {
	// 		draggable: false,
	// 		editable:false,
	// 		mapTypeControl: false,
	// 		zIndex:1
	// 	};

	// 	//added code for polygon boudary area initial
	// 	if(typeof(this.polygonBoundaryArea) == 'undefined'){//set polygon options if it does not exist already
	// 		this.polygonBoundaryArea = new google.maps.Polygon({
	// 			//path: polygonBoundaryCoords,
	// 			draggable: true,
	// 			editable: true,
	// 			strokeColor: '#4fff2f',
	// 			strokeOpacity: 1.0,
	// 			strokeWeight: 1,
	// 			fillColor: '#4fff2f',
	// 			fillOpacity: 0.3,
	// 			zIndex:3
	// 		});
	// 	}

	// 	for(let i=0;i<this.polygonBoundaryArea.length;i++){
	// 		this.polygonBoundaryArea[i].setOptions(mapBoundaryOpt);
	// 	}

	// 	//polygonBoundaryArea.setOptions(mapBoundaryOpt);
	// 	let mapExclisionOpt = {
	// 			draggable: true,
	// 			editable: true,
	// 			zIndex:3
	// 	};

	// 	if(typeof(this.polygonExclusionAreas) != 'undefined'){//checking if area exist
	// 		for(let i=0;i<this.polygonExclusionAreas.length;i++){
	// 			this.polygonExclusionAreas[i].setOptions(mapExclisionOpt);
	// 		}
	// 	}

	// 	this.finished('boundary');
	// }

	// private finished(from){//finish editing polygons
	// 	this.closeAllInfoWindows();//close all info windows
	// 	if(from == 'boundary'){
	// 		this.delBoundary = false;
	// 		this.addBoundarySetback = false;
	// 	}else{
	// 		this.delExclusion = false;
	// 		this.addExclusionSetback = false;
	// 	}
	// }

	// private closeAllInfoWindows() {//cloase all infowindows on finish
	// 	for (var i=0;i<this.infoWindows.length;i++) {
	// 		this.infoWindows[i].close();
	// 	}
	// 	this.infoWindows = [];//reset infowindows array
	// }
}