import { Component, OnInit,EventEmitter } from '@angular/core';
// import { Router } from '@angular/router';
import {ProjectService} from '../projects/projects.service';
import { SharedService } from '../shared/utils.service';
// import { MapService } from '../shared/map.service';

@Component({
    templateUrl: './app/projects/projects.Component.html'
})

export class ProjectsComponent implements OnInit {

    constructor(
        private projectService: ProjectService,private sharedService : SharedService){}

	ngOnInit() {
        // reset login status
        this.getProjects();
        // this.mapService.loadDependcy();
        // let temp : any = this.mapService.get();
        // console.log("MapService",temp);

    }
    private projects : any = []; 
    private  myFocusTriggeringEventEmitter  = new EventEmitter<boolean>();
    private searchFilter : any;
    private id : any;
    private adminName : string;
    private getProjects(): any {
        let user = this.sharedService.get();
		if(user._id){
            // this.id = "5832e0b253c7240a4cb6d8f7";
            this.projectService.getUserProjects(user._id).subscribe(proj =>{
                let response = JSON.parse(proj._body);
                this.projects = response.projects;
                this.adminName = response.admin.first_name+' '+response.admin.last_name;
                console.log(this.projects);
            },
            err => {
                console.log(err)
            });
	        // ProjectService.getUserProjects($rootScope._id).success(function(response){
	        //     $scope.projects = response.projects;//fetch user's all projects
	        //     $scope.adminName = response.admin.first_name+' '+response.admin.last_name;
	        //     $scope.projectsLoaded = true;
	        // }).error(function(error){
	        //     console.log("error:"+error);
	        // });
	    }else{
            window.location.href="/#/";
	    }
	}

    report_url() : any {

    }
     setFocus() : any {
        this.myFocusTriggeringEventEmitter.emit(true);
     }
     archiveProject() : any {}

}