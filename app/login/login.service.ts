import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/operator/map';
import { User } from '../shared/user.schema';
@Injectable()
export class LoginService {
	constructor (private http: Http) {}

	login(user : User) : any {
		var headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');	
		var data = 'username=' + user.email.toLowerCase() + '&password=' + user.password + ''
		return this.http.post('/api/users/login',data,{headers : headers});
	}

	getUserInfo(user_id : string) : any {
		
	}
}