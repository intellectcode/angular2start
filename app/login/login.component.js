"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var login_service_1 = require('../login/login.service');
var forms_1 = require('@angular/forms');
var validation_service_1 = require('../shared/validation.service');
var user_schema_1 = require('../shared/user.schema');
var angular_2_local_storage_1 = require('angular-2-local-storage');
var utils_service_1 = require('../shared/utils.service');
// import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms'
var LoginComponent = (function () {
    function LoginComponent(fb, loginService, localStorageService, window, sharedService) {
        this.loginService = loginService;
        this.localStorageService = localStorageService;
        this.sharedService = sharedService;
        this.user = new user_schema_1.User(); //User schema
        this.complexForm = fb.group({
            'email': [null, forms_1.Validators.compose([forms_1.Validators.required, validation_service_1.ValidationService.checkEmailFormat])],
            'password': [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function (form_date) {
        var _this = this;
        this.loginService.login(form_date).subscribe(function (data) {
            var token = JSON.parse(data._body).token;
            if (token) {
                _this.registeredSucessfully = true;
                _this.registrationError = "";
                _this.setUserStatus(token); //call login controller's method to set the login status
                var domain = _this.user.email.replace(/.*@/, "");
                var index = _this.user.email.replace(/.*@/, "").indexOf('.'); //indx of company name
                var companyName = domain.substring(index, 0); //company name
                if (companyName == 'hstsolar') {
                    _this.sharedService.addAttr("isHstAdmin", true);
                    _this.localStorageService.set("isHstAdmin", true);
                }
                window.location.href = "/#/projects";
            }
        }, function (err) {
            console.log(err);
        });
    };
    LoginComponent.prototype.setUserStatus = function (token) {
        this.loggedInSucessfully = true; //set the logged in params
        this.localStorageService.set("mean-token", token);
        this.currentUser();
    };
    LoginComponent.prototype.currentUser = function () {
        var token;
        token = this.localStorageService.get("mean-token");
        if (token) {
            var payload = token.split('.')[1];
            payload = window.atob(payload);
            payload = JSON.parse(payload);
            // let full_name = payload.first_name + " " + payload.last_name;
            this.sharedService.set(payload);
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: './app/login/login.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, login_service_1.LoginService, angular_2_local_storage_1.LocalStorageService, Window, utils_service_1.SharedService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map