import { Component, OnInit} from '@angular/core';
import { LoginService} from '../login/login.service';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { ValidationService } from '../shared/validation.service';
import { User } from '../shared/user.schema';

import { LocalStorageService } from 'angular-2-local-storage';
import { SharedService } from '../shared/utils.service';
// import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms'
@Component({
	templateUrl : './app/login/login.component.html'
	// directives : [REACTIVE_FORM_DIRECTIVES]
})

export class LoginComponent implements OnInit {
	private complexForm : FormGroup;
	private user = new User(); //User schema
	private registeredSucessfully;
	private registrationError;
	private loggedInSucessfully;
	ngOnInit(){
		
	}
	constructor(fb: FormBuilder,private loginService: LoginService,private localStorageService: LocalStorageService,window : Window, private sharedService : SharedService){
	    this.complexForm = fb.group({
	      'email' : [null, Validators.compose([Validators.required,ValidationService.checkEmailFormat])],
	      'password': [null, Validators.compose([Validators.required, Validators.minLength(8)])]
    	})
	}

	private login(form_date : any) : void{
		this.loginService.login(form_date).subscribe(data => {
			let token = JSON.parse(data._body).token;
			if(token){
				this.registeredSucessfully = true;
                this.registrationError = "";
                this.setUserStatus(token);//call login controller's method to set the login status
                let domain  = this.user.email.replace(/.*@/, "");
                let index   = this.user.email.replace(/.*@/, "").indexOf('.');//indx of company name
                var companyName = domain.substring(index,0);//company name
                if(companyName == 'hstsolar'){//id hst user set admin rights
                   this.sharedService.addAttr("isHstAdmin",true);
                   this.localStorageService.set("isHstAdmin",true);   
                }
                window.location.href="/#/projects";
			}
		},
		 err => {
			 console.log(err)
		});
	}

	private setUserStatus(token:string) : void {
        this.loggedInSucessfully = true; //set the logged in params
        this.localStorageService.set("mean-token",token);	
        this.currentUser();
    }		

    private currentUser(): void {
    	let token;
    	token = this.localStorageService.get("mean-token");
    	if (token) {
            let payload = token.split('.')[1];
            payload = window.atob(payload);
            payload = JSON.parse(payload);
            // let full_name = payload.first_name + " " + payload.last_name;
            this.sharedService.set(payload);
        }
    }
}